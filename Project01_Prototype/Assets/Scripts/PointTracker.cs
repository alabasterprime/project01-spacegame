﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PointTracker : MonoBehaviour
{

    public float currentScore;
    public float moonScore = 100.0f;

    public Text scoreText;

    // Use this for initialization
    void Start()
    {
        currentScore = 0.0f;
        SetCountText();
    }

    public void AddScore()
    {
        currentScore += moonScore;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Moon")
        {
            AddScore();
            SetCountText();
        }
    }

    void SetCountText()
    {
        scoreText.text = "Current Score: " + currentScore.ToString();
    }
}