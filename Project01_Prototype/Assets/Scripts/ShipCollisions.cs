﻿using UnityEngine;
using System.Collections;

public class ShipCollisions : MonoBehaviour {

    public GameObject explosion;
    public bool Dead;

    void Start()
    {
        Dead = false;
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Boundary" || col.gameObject.tag == "Planet" || col.gameObject.tag == "Sun")
        {
            Destroy(gameObject);
            Instantiate(explosion, transform.position, transform.rotation);
            Dead = true;
        }
    }
}
