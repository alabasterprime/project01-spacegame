﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Sun : MonoBehaviour 
{
    [Tooltip("Speed this planet rotates at.")]
    public float rotSpeed;
    [Tooltip("Diameter of this planet.")]
    public float diameter;

	void Start () 
    {
        InitializePosition();
        InitializeRadius();
	}
	
	void Update () 
    {
#if UNITY_EDITOR
        InitializeRadius();
#endif
        Rotate();
	}

    protected void InitializeRadius()
    {
        Vector3 size = new Vector3(diameter, diameter, diameter);
        transform.localScale = size;
    }

    protected void Rotate()
    {
        transform.Rotate(Vector3.up * rotSpeed * Time.deltaTime);
    }

    public virtual void InitializePosition()
    {
        transform.position = Vector3.zero;
    }
}
