﻿using UnityEngine;
using System.Collections;


public class Planet_Simple : MonoBehaviour 
{
    [SerializeField]
    GameObject orbitBody;

    public float mass;
    public float diameter;
    public float rotationSpeed;

    public float surfaceDistance;
    public float orbitSpeed;

    public bool simplify;

	// Use this for initialization
	void Start () 
    {
        InitializeDiameter();
        InitializePosition();
	}
	
	// Update is called once per frame
	void Update () 
    {
        Rotate();
        Orbit();
	}

    void InitializeDiameter()
    {
        Vector3 size = new Vector3(diameter, diameter, diameter);
        transform.localScale = size;
    }

    void InitializePosition()
    {
        if (gameObject.tag.Equals("Sun"))
            transform.position = Vector3.zero;
        else
        {
            if (simplify)
                transform.position = new Vector3(0, 0, GetRadius());
            else
            {
                Vector3 parentPos = orbitBody.transform.position;
                float initX, initY, initZ;
                float initAngle = Random.Range(0, 360);

                initX = GetRadius() * Mathf.Cos(initAngle);
                initY = 0;
                initZ = GetRadius() * Mathf.Sin(initAngle);

                Vector3 initPos = parentPos + new Vector3(initX, initY, initZ);
                transform.position = initPos;
            }
        }
    }

    void Rotate()
    {
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }

    void Orbit()
    {
        transform.RotateAround(orbitBody.transform.position, Vector3.up, orbitSpeed * Time.deltaTime);
    }

    public float GetRadius()
    {
        float parentRad = (orbitBody.transform.localScale.x + orbitBody.transform.localScale.y + orbitBody.transform.localScale.z) / 3 / 2;
        float radius = diameter / 2;
        return parentRad + radius + surfaceDistance;
    }
}
