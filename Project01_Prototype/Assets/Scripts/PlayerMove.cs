﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

    public int moveSpeed = 30;
    public int rotationSpeed = 75;
    public int boostSpeed = 100;
    public float boostTimer = 0;
    public float boostCooldown = 0;
    public bool boostBlocked = false;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.forward * (Time.deltaTime * moveSpeed));
        if (Input.GetKey("w"))
        {
            transform.Translate(Vector3.up * (Time.deltaTime * moveSpeed));
        }
        if (Input.GetKey("s"))
        {
            transform.Translate(Vector3.down * (Time.deltaTime * moveSpeed));
        }
        if (Input.GetKey("a"))
        {
            transform.Rotate(Vector3.down * (Time.deltaTime * rotationSpeed));
        }
        if (Input.GetKey("d"))
        {
            transform.Rotate(Vector3.up * (Time.deltaTime * rotationSpeed));
        }
        if (Input.GetButton("Fire1"))
        {
            Boosto();
        }
        if (boostBlocked == true)
        {
            moveSpeed = 30;
            boostCooldown += Time.deltaTime;
            if (boostCooldown >= 5)
            {
                boostBlocked = false;
                boostTimer = 0;
                boostCooldown = 0;
            }
        }
    }
    public void Boosto()
    {
        moveSpeed = boostSpeed;
        boostTimer += Time.deltaTime;
        if (boostTimer >= 1)
        {
            boostBlocked = true;
        }
        
    }
}

