-------
Music
-------

"Inspired" Kevin MacLeod (incompetech.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/

----------
Textures
----------

Planet textures by James Hastings-Trew (planetpixelemporium.com)
Licensed under Creative Commons: By Attribution 3.0 License
http://creativecommons.org/licenses/by/3.0/